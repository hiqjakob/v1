{
  "rules": {
    "align": [                              /* enforces vertical alignment */
      true,
      "parameters",
      "statements",
      "arguments"
    ],
    "ban": false,                           /* Bans the use of specific functions or global methods */
    "class-name": true,                     /* Enforces PascalCased class and interface names. */ 
    "comment-format": [                     /* Enforcing formatting rules for single-line comments*/
      true,
      "check-space"                         /* Single line comment start with space as in // comment*/
    ],
    "curly": true,                          /* Enforces braces for if/for/do/while statements */
    "eofline": true,                        /* Ensures the file ends with a newline. */
    "forin": true,                          /* Requires a for ... in statement to be filtered with an if statement. */
    "indent": [                             /* Enforces indentation with tabs or spaces. */
      true,
      "spaces"
    ],
    "interface-name": [                     /* Requires interface names to begin with a capital ‘I’ */
      false, 
      "always-prefix"
    ],        
    "jsdoc-format": true,                   /* Enforces basic format rules for JSDoc comments. */
    "label-position": true,                 /* Only allows labels in sensible locations. This rule only allows labels to be on do/for/while/switch */
    "max-line-length": [                    /* Requires lines to be under a certain max length. */
      true,
      180
    ],
    "member-access": [                      /* Requires explicit visibility declarations for class members. Makes code more readable */
      false,
      "check-accessor",
      "check-constructor"
    ],       
    "member-ordering": [                    /* Enforces member ordering. */
      false,
      {"order" : [
      /*public-static-field
        public-static-method
        protected-static-field
        protected-static-method
        private-static-field
        private-static-method
        public-instance-field
        protected-instance-field
        private-instance-field
        public-constructor
        protected-constructor
        private-constructor
        public-instance-method
        protected-instance-method
        private-instance-method 
      */
      ]}
    ],
    "no-any": false,                        /* Disallows usages of any as a type declaration. */
    "no-arg": true,                         /* Disallows use of arguments.callee */
    "no-bitwise": false,
    "no-conditional-assignment": false,     /* Disallows any type of assignment in conditionals. This applies to do-while, for, if, and while statements.*/
    "no-consecutive-blank-lines": [         /* Disallows one or more blank lines in a row. */
      true,
      1
    ],    
    "no-console": [                         /* Bans the use of specified console methods. */
      false
    ],
    "no-construct": true,                   /* Disallows access to the constructors of String, Number, and Boolean. */
    "no-debugger": true,                    /* Disallows debugger statements. */
    "no-duplicate-variable": true,          /* Disallows duplicate variable declarations in the same block */
    "no-duplicate-super": true,             /* Warns if ‘super()’ appears twice in a constructor. */
    "no-empty": true,                       /* Disallows empty blocks. */  
    "no-inferrable-types": [                /* Disallows explicit type declarations for variables or parameters initialized to a number, string, or boolean. */
      false
    ],           
    "no-internal-module": true,             /* Disallows internal module, Use the newer namespace keyword instead. */ 
    "no-null-keyword": true,                /* Disallows use of the null keyword literal. Ensures that only undefined is used */
    "no-require-imports": true,             /* Disallows invocation of require(). Prefer the newer ES6-style imports over require().*/
    "no-shadowed-variable": true,           /* Disallows shadowing variable declarations. Shadowing a variable masks access to it and obscures to what value an identifier actually refers. */
    "no-string-literal": false,             /* Disallows object access via string literals. */
    "no-switch-case-fall-through": true,    /* Fall though in switch statements is often unintentional and a bug.  fall through is allowed when case statements are consecutive or a magic \/* falls through *\/ */
    "no-trailing-whitespace": true,         /* Disallows trailing whitespace at the end of a line. */
    "no-unused-expression": false,          /* Disallows unused expression statements. */
    "no-use-before-declare": true,          /* Disallows usage of variables before their declaration. */
    "no-var-keyword": true,                 /* Disallows usage of the var keyword. */
    "no-var-requires": false,               /* Disallows the use of require statements except in import statements.  use ES6 style imports */
    "object-literal-sort-keys": false,      /* Requires keys in object literals to be sorted alphabetically */
    "one-line": [                           /* Requires the specified tokens to be on the same line as the expression preceding them. */
      true,
      "check-open-brace",
      "check-catch",
      "check-else",
      "check-whitespace",
      "check-finally"
    ],
    "quotemark": [                          /* Requires single or double quotes for string literals. */
      true,
      "single",
      "jsx-double",
      "avoid-escape"
    ],
    "radix": false,                          /* Requires the radix parameter to be specified when calling parseInt. */     
    "switch-default": true,                 /* Require a default case in all switch statements. */
    "trailing-comma": [                     /* Requires or disallows trailing commas in array and object literals, destructuring assignments, function and tuple typings, named imports and function parameters.*/
      false,
      {
        "multiline": "always", 
        "singleline": "always" 
      }
    ],
    "triple-equals": [                      /* equires === and !== in place of == and !=. */
      true,
      "allow-null-check", 
      "allow-undefined-check" 
    ],
    "typedef": [                            /* Requires type definitions to exist.*/
      true, 
      /*"call-signature",                   /* checks return type of functions. */
      "parameter",                          /* checks type specifier of function parameters for non-arrow functions. */
      /*"arrow-parameter",                  /* checks type specifier of function parameters for arrow functions. */
      "property-declaration",               /* checks return types of interface properties. */
      "variable-declaration",               /* checks non-binding variable declarations. */
      "member-variable-declaration"         /* checks member variable declarations. */

    ],
    "typedef-whitespace": [                 /* Requires or disallows whitespace for type definitions. */
      true,
      {
        "call-signature": "nospace",
        "index-signature": "nospace",
        "parameter": "nospace",
        "property-declaration": "nospace",
        "variable-declaration": "nospace"
      }
    ],
    "variable-name": [                    /* Checks variable names for various errors. */
      true,
      "allow-leading-underscore",
      "ban-keywords"
    ],
    "whitespace": [
      true,
      "check-branch",
      "check-decl",
      "check-operator",
      "check-separator",
      "check-type"
    ],
    "semicolon" : [
      true, 
      "always"
    ]
  }
}