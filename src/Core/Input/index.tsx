import * as React from 'react';
import * as spacing from 'Settings/spacing';
import * as _ from 'lodash';
import { Row2Col1 } from 'Styles/Grid';


type Props = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement> & {
  label?: string;
  myOnChange?: (targetValue: string) => void;
  actualValue?: any;  // validation
  labelPosition: 'above' | 'aside';
  noDebounce?: boolean;
};


class Input extends React.Component<Props>  {
  input: HTMLInputElement;

  constructor(props: Props) {
    super(props);

  }

  shouldComponentUpdate(nextProps: Props) {
    let valueHasUpdated: boolean = nextProps.actualValue !== this.props.actualValue;
    let labelHasUpdated: boolean = nextProps.label !== this.props.label;

    let shouldUpdate = valueHasUpdated || labelHasUpdated;
    return shouldUpdate;
  }

  // Debounce
  delayedCallback = _.debounce(function () {
    this.props.myOnChange(this.input.value);
  }, 500)

  onChange(event: React.SyntheticEvent<HTMLInputElement>) {
    event.persist();
    this.input.value = event.currentTarget.value
    this.props.noDebounce ? this.props.myOnChange(event.currentTarget.value) : this.delayedCallback();
  }

  componentDidMount() {
    // bara för att få ett startvärde för textinputsen, så att den inte står tom i början 
    if (this.props.actualValue && this.props.type === "number") {
      this.input.value = this.props.actualValue;
    }
  }

  getLabelPosition(): React.CSSProperties {
    switch (this.props.labelPosition) {
      case 'above':
        return Row2Col1
      case 'aside':
        return {}
      default:
        return Row2Col1
    }

  }

  render() {
    let { label, labelPosition } = this.props;
    // console.log('render - input for ' + label)
    let inputProps = {
      ...this.props
    };
    delete inputProps.myOnChange;
    delete inputProps.label;
    delete inputProps.actualValue;
    delete inputProps.labelPosition;
    delete inputProps.noDebounce;

    return (
      <label style={{
        textTransform: labelPosition === 'above' ? 'uppercase' : 'none',
        marginBottom: spacing.MEDIUM,
        ...(this.getLabelPosition())
      }}>
        {label}
        < input
          name="name"
          {...inputProps }
          onChange={(event) => this.onChange(event)}
          ref={(input: HTMLInputElement) => {
            this.input = input;
          }}
          style={{
            outline: 'none',
            padding: spacing.MEDIUM,
            borderRadius: spacing.MEDIUM,
            margin: labelPosition === 'above' ? spacing.NONE : spacing.MEDIUM
          }}
        />
      </label >
    );
  }

}
export default Input
