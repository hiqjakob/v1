/* global localStorage */

export interface IAuthInfo {
  token: string;
  getHeaders: () => any;
}

class AuthInfo implements IAuthInfo {
  token: string;

  constructor() {
    this.token = '';
  }

  getHeaders() {
    return {
      AUTHORIZATION: 'Bearer ' + this.token,
    };
  }

  //   set(token) {
  //     this.token = token
  //     localStorage.setItem('token', token)
  //   }

  //   setCurrentUser(currentUser) {
  //     this.currentUser = currentUser
  //     localStorage.setItem('currentUser', currentUser)
  //   }

  //   enableAutoLogin() {
  //     this.autologin = true
  //     localStorage.setItem('autologin', true)
  //   }
  //   disableAutoLogin() {
  //     this.autologin = false
  //     localStorage.setItem('autologin', false)
  //   }

  //   getAutoLogin() {
  //     if (this.autologin === '') {
  //       this.autologin = localStorage.getItem('autologin') === 'true'
  //     }
  //     return this.autologin
  //   }

  //   getCachedInfo() {
  //     if (!this.getAutoLogin()) {
  //       return {
  //         token: '',
  //         currentUser: '',
  //       }
  //     }

  //     return {
  //       token: localStorage.getItem('token'),
  //       currentUser: localStorage.getItem('currentUser'),
  //     }
  //   }
}

// let authInfo: IAuthInfo = new AuthInfo();

export default AuthInfo;
