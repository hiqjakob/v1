export function get(url: string, headers?: string[][]) {
  return request(url, 'get', undefined, headers);
}

export function getJson(url: string, headers?: string[][]) {
  return request(url, 'get', headers).then(r => r.json());
}

export async function getJsonAsync(url: string, headers?: string[][]) {
  return request(url, 'get', headers).then(r => r.json());
}

export function post(url: string, body: any = undefined, headers?: string[][]) {
  return request(url, 'post', body, headers);
}

export function postJson(url: string, body: any = undefined, headers?: string[][]) {
  return request(url, 'post', body, headers).then(r => r.json());
}

export function remove(url: string, body: any = undefined, headers?: string[][]) {
  return request(url, 'delete', body, headers);
}

export function put(url: string, body?: any, headers?: string[][]) {
  return request(url, 'put', body, headers);
}

export function putJson(url: string, body: any = undefined, headers?: string[][]) {
  return request(url, 'put', body, headers).then(r => r.json());
}

type HttpMethod = 'get' | 'post' | 'delete' | 'put';

type RequestError = {
  key: string;
  message: string;
};

function request(url: string, method: HttpMethod, body?: any, headers?: string[][]): Promise<any> {

  let requestHeaders: any = new Headers();
  requestHeaders.append('X-Requested-With', 'XMLHttpRequest');
  requestHeaders.append('Content-Type', 'application/json');
  if (headers) {
    headers.map((value) => {
      requestHeaders.append(value[0], value[1]);
    });
  }
  let returnRequest: any = fetch(
    url,
    {
      headers: requestHeaders,
      credentials: 'include',
      method, mode: 'cors',
      body: body !== undefined ? JSON.stringify(body) : undefined,
    }
  ).then<any>(r => {
    if (r.status >= 200 && r.status < 400) {
      return Promise.resolve(r);
    } else if (r.status === 400) {
      return handleBadRequest(r);
    } else {
      return Promise.reject(
        { status: r.status, }
      );
    }
  });
  return returnRequest;
}

function handleBadRequest(response: any) {
  return response.json().then(body => {
    let errors: RequestError[] = [];
    if (body.modelState) {
      for (let propertyName in body.modelState) {
        if (body.modelState.hasOwnProperty(propertyName)) {
          errors.push({
            key: propertyName,
            message: body.modelState[propertyName][0]
          });
        }
      }
    }
    return Promise.reject({
      status: response.status,
      statusText: response.statusText,
      body: body,
      errors: errors
    });
  });
}