
const ACTUAL_API_BASE_URL: string = 'http://localhost:8080/api';

import { getJsonAsync } from './apiHelpers';
export const getUsers: any = getJsonAsync(ACTUAL_API_BASE_URL + '/users');

export default {
  getUsers
};
