import { LANGUAGE } from 'Store/ducks//settings'
import { store } from 'App'

export type Translations = {
  MENU_GAME: string;
  GAME_CONTROL_SIZE_LABEL: string;
  GAME_CONTROL_SWEDISH: string;
  GAME_CONTROL_ENGLISH: string;
  GAME_CONTROL_SQUARE: string;
  GAME_CONTROL_CIRCLE: string;
  GAME_CONTROL_BOARD_SIZE: string;

  GAME_CONTROL_PLAYER_MOVES_LABEL: string;
  GAME_CONTROL_PLAYER_MOVES_NO_MOVES: string;
  GAME_CONTROL_PLAYER_TURN_RIGHT: string;
  GAME_CONTROL_PLAYER_TURN_LEFT: string;
  GAME_CONTROL_PLAYER_MOVE: string;
  GAME_CONTROL_PLAYER_START_MOVING: string;
  GAME_CONTROL_PLAYER_START_X: string;
  GAME_CONTROL_PLAYER_START_Y: string;
};

const swedish: Translations = {
  MENU_GAME: 'Spel',

  GAME_CONTROL_SIZE_LABEL: 'box storlek (px)',
  GAME_CONTROL_SWEDISH: 'Svenska',
  GAME_CONTROL_ENGLISH: 'Engelska',
  GAME_CONTROL_SQUARE: 'Fyrkantigt',
  GAME_CONTROL_CIRCLE: 'Cirkel',
  GAME_CONTROL_BOARD_SIZE: 'Brädets storlek',
  GAME_CONTROL_PLAYER_MOVES_LABEL: 'Lista med drag',
  GAME_CONTROL_PLAYER_MOVES_NO_MOVES: 'Inga drag',
  GAME_CONTROL_PLAYER_TURN_RIGHT: 'H',
  GAME_CONTROL_PLAYER_TURN_LEFT: 'V',
  GAME_CONTROL_PLAYER_MOVE: 'G',
  GAME_CONTROL_PLAYER_START_MOVING: 'STARTA',
  GAME_CONTROL_PLAYER_START_X: 'startX',
  GAME_CONTROL_PLAYER_START_Y: 'startY'

}

const english: Translations = {
  MENU_GAME: 'game',

  GAME_CONTROL_SIZE_LABEL: 'box size (px)',
  GAME_CONTROL_SWEDISH: 'Swedish',
  GAME_CONTROL_ENGLISH: 'English',
  GAME_CONTROL_SQUARE: 'Square',
  GAME_CONTROL_CIRCLE: 'Circle',
  GAME_CONTROL_BOARD_SIZE: 'Board size',
  GAME_CONTROL_PLAYER_MOVES_LABEL: 'Move-list',
  GAME_CONTROL_PLAYER_MOVES_NO_MOVES: 'No Moves',
  GAME_CONTROL_PLAYER_TURN_RIGHT: 'R',
  GAME_CONTROL_PLAYER_TURN_LEFT: 'L',
  GAME_CONTROL_PLAYER_MOVE: 'F',
  GAME_CONTROL_PLAYER_START_MOVING: 'START',
  GAME_CONTROL_PLAYER_START_X: 'startX',
  GAME_CONTROL_PLAYER_START_Y: 'startY'

}


const translate = (key: string, language: LANGUAGE = store.getState().settings.language) => {
  let value: string;

  switch (language) {
    case LANGUAGE.SWEDISH:
      value = swedish[key];
      break;
    case LANGUAGE.ENGLISH:
      value = english[key];
      break;
    default:
      value = english[key]
  }

  if (!value) {
    if (Number['isNaN'] && !Number.isNaN(parseInt(key, 10))) {
      //console.warn('acceptable missing key', key)
      return key
    }

    console.error('Missing translation for key:', key)
    value = key
  }
  return value
}

export default translate;