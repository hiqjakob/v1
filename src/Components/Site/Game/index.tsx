import * as React from 'react';
import { Row1Col2 } from 'Styles/Grid';
import Section from 'Components/Shared/SiteLayout/Section';
import * as spacing from 'Settings/spacing';
import Board from 'Components/Shared/Board';
import Control from 'Components/Shared/Control';

class Game extends React.Component {

  render() {
    return (
      <div>
        <Section sectionType="SECTION">
          <div style={{ ...Row1Col2, padding: spacing.HUGE }}>
            <Board />
            <Control />
          </div>

        </Section>
      </div>
    );
  }
}
export default Game;
