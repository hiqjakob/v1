import * as React from 'react';
import { connect } from 'Store';
import { setLanguage, setSize, setBoardSettings, LANGUAGE, IState as Settings } from 'Store/ducks/settings';
import { getPlayer, movePlayer, IState as Player } from 'Store/ducks/player';
import { getBoard } from 'Store/ducks/game';
import { NETWORK_STATE, } from 'Store/ducks/constants';
import * as spacing from 'Settings/spacing';
import t from 'Infrastructure/translations';
import Input from 'Core/Input';
import Loader from 'Components/Shared/Loader';
import * as _ from 'lodash'
import { Row1Col4 } from 'Styles/Grid';

type Props = {
  setLanguage?: (lanugage: LANGUAGE) => void;
  setSize?: (size: number) => void;
  getBoard?: (boardSettings: Api.BoardSettings) => void;
  setBoardSettings?: (boardSettings: Api.BoardSettings) => void;
  getPlayer?: (player?: Api.Player) => void;
  movePlayer?: (turn: Api.Turn) => void;
  player?: Player;

  settings?: Settings;
};

type State = {
  moves?: string;
  isLoading?: boolean;
};

class Control extends React.Component<Props, State> {
  moveKeys: string[];

  constructor(props: Props) {
    super(props);

    this.state = {
      isLoading: false,
      moves: ''
    }
    this.moveKeys = ['GAME_CONTROL_PLAYER_TURN_LEFT', 'GAME_CONTROL_PLAYER_MOVE', 'GAME_CONTROL_PLAYER_TURN_RIGHT']
    // console.log('Control CON - isLoading: true ')

  }

  componentWillReceiveProps(nextProps: Props) {
    let { settings, player } = nextProps
    let setSizeLoading = settings.network.setSize === NETWORK_STATE.LOADING;
    let setLanguageLoading = settings.network.setLanguage === NETWORK_STATE.LOADING;
    let setBoardSettingsLoading = settings.network.setBoardSettings === NETWORK_STATE.LOADING;
    let getPlayerLoading: boolean = player.network.get === NETWORK_STATE.LOADING;
    let getPlayerMovingLoading: boolean = player.network.move === NETWORK_STATE.LOADING;

    let isLoading: boolean = setSizeLoading || setLanguageLoading || setBoardSettingsLoading || getPlayerLoading || getPlayerMovingLoading;
    if (!_.isEqual(nextProps.settings.language, this.props.settings.language)) {
      this.translate(this.props.settings.language, nextProps.settings.language);
    }
    this.setState({ isLoading })
  }

  shouldComponentUpdate(nextProps: Props, nextState: State) {
    false && console.log(nextState); // Mina regler ställer till det annars

    let boardSettingsUpdated: boolean = !_.isEqual(nextProps.settings.boardSettings, this.props.settings.boardSettings);
    let languageUpdated: boolean = !_.isEqual(nextProps.settings.language, this.props.settings.language);
    let sizeUpdated: boolean = !_.isEqual(nextProps.settings.size, this.props.settings.size);

    let movesUpdated: boolean = !_.isEqual(nextState.moves, this.state.moves);
    // let isLoadingHasChanged: boolean = nextState.isLoading !== this.state.isLoading;
    let shouldUpdate: boolean = (boardSettingsUpdated || languageUpdated || sizeUpdated || movesUpdated);

    return shouldUpdate;
  }

  onSizeChange = (size: string) => {
    this.props.setSize(Number(size));
  }

  onCountryChange = (language: string) => {
    this.props.setLanguage(Number(language));
  }

  onBoardTypeChange = (type: Api.BoardType) => {
    let newSettings: Api.BoardSettings = {
      ...this.props.settings.boardSettings, type
    }
    this.props.getBoard(newSettings);
    this.props.setBoardSettings(newSettings);
  }

  onBoardSizeChange = (size: string) => {
    // console.log('boardChanging')
    let newSettings: Api.BoardSettings = {
      ...this.props.settings.boardSettings, size: Number(size)
    }
    this.props.getBoard(newSettings);
    this.props.setBoardSettings(newSettings);
  }


  startMoving = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    let { currentTurn } = this.props.player.player
    this.state.moves.split('').forEach((move: string, index: number) => {
      let myMove: Api.Turn = this.moveToTurn(move, index, currentTurn);
      currentTurn = myMove;
      console.log('turn: ' + myMove.turn + ', looking: ' + myMove.looking)
      console.log(myMove.position)
      this.props.movePlayer(myMove)
    })

  }

  moveToTurn(move: string, index: number, currentTurn: Api.Turn): Api.Turn {
    let turn: Api.Turn;
    let moveKey: string = '';

    for (let i: number = 0; i < this.moveKeys.length; i++) {
      if (_.isEqual(move, t(this.moveKeys[i]))) {
        moveKey = this.moveKeys[i];
      }
    }
    console.log(moveKey)

    turn = {
      looking: moveKey !== 'GAME_CONTROL_PLAYER_MOVE' ? this.getNewDirection(currentTurn.looking, moveKey) : currentTurn.looking,
      position: moveKey === 'GAME_CONTROL_PLAYER_MOVE' ? this.getNewPosition(currentTurn.position, currentTurn.looking) : currentTurn.position,
      turn: index + 1
    }

    return turn;
  }

  getNewPosition(startPosition: Api.Position, looking: Api.Direction): Api.Position {
    let newPosition: Api.Position;
    let { y, x } = startPosition;
    switch (looking) {
      case 'north':
        y = y + 1;
        break;
      case 'east':
        x = x + 1;
        break;
      case 'south':
        y = y + -1;
        break;
      case 'west':
        x = x + -1;
        break;
    }

    newPosition = { x, y }
    return newPosition;
  }
  getNewDirection(looking: Api.Direction, moveKey): Api.Direction {
    let newDirection: Api.Direction = looking;
    switch (looking) {
      case 'north':
        if (moveKey = 'GAME_CONTROL_PLAYER_TURN_LEFT') {
          newDirection = 'west'
        } else if (moveKey = 'GAME_CONTROL_PLAYER_TURN_RIGHT') {
          newDirection = 'east'
        }
        return newDirection;
      case 'east':
        if (moveKey = 'GAME_CONTROL_PLAYER_TURN_LEFT') {
          newDirection = 'north'
        } else if (moveKey = 'GAME_CONTROL_PLAYER_TURN_RIGHT') {
          newDirection = 'south'
        }
        return newDirection;
      case 'south':
        if (moveKey = 'GAME_CONTROL_PLAYER_TURN_LEFT') {
          newDirection = 'east'
        } else if (moveKey = 'GAME_CONTROL_PLAYER_TURN_RIGHT') {
          newDirection = 'west'
        }
        return newDirection;
      case 'west':
        if (moveKey = 'GAME_CONTROL_PLAYER_TURN_LEFT') {
          newDirection = 'south'
        } else if (moveKey = 'GAME_CONTROL_PLAYER_TURN_RIGHT') {
          newDirection = 'north'
        }
        return newDirection;
    }
  }


  addMove = (event: React.MouseEvent<HTMLButtonElement>, move: string) => {
    event.preventDefault()
    let { moves } = this.state
    moves = `${moves}${move}`
    console.log(moves)
    this.setState({ moves });
  }

  onPlayerChange = () => {
    console.log('startMoving()')
  }

  translate(fromLanguage: LANGUAGE, toLanguage: LANGUAGE) {
    let { moves } = this.state;
    let moveList: string[] = moves.split('');
    let newMoves: string = '';

    moveList.forEach((move: string) => {
      for (let i: number = 0; i < this.moveKeys.length; i++) {
        let moveKey = this.moveKeys[i];
        if (_.isEqual(move, t(moveKey, fromLanguage))) {
          newMoves = newMoves + t(moveKey, toLanguage)
          return;
        }
      }
    })

    this.setState({ moves: newMoves })

  }
  renderMoves(moveName: string, index: number) {
    return (
      <button type="button"
        style={{
          padding: spacing.MEDIUM,
          border: '2px solid indianred',
          borderRadius: '2px',
          cursor: 'pointer'
        }}
        key={index}
        onClick={(e) => this.addMove(e, t(moveName))}>
        {t(moveName)}
      </button>
    )
  }

  render() {
    let dimension: string = '600px';
    let circle: Api.BoardType = 'circle';
    let square: Api.BoardType = 'square';
    let { settings } = this.props;


    return (
      <div style={{
        height: dimension,
        maxHeight: dimension,
        backgroundColor: 'PeachPuff',
        border: '2px solid indianred',
        borderRadius: '4px',
        cursor: 'pointer',
        padding: spacing.MEDIUM
      }}
      >
        {this.state.isLoading ?
          <Loader />
          :
          <div>
            <Input
              type="number"
              myOnChange={this.onSizeChange}
              label={t('GAME_CONTROL_SIZE_LABEL')}
              actualValue={settings.size}
              labelPosition="above" />
            <div>
              <Input
                type="radio"
                name="language"
                myOnChange={this.onCountryChange}
                label={t('GAME_CONTROL_SWEDISH')}
                actualValue={settings.language}
                labelPosition="aside"
                value={LANGUAGE.SWEDISH}
                checked={settings.language === LANGUAGE.SWEDISH}
                noDebounce />
              <Input
                type="radio"
                name="language"
                myOnChange={this.onCountryChange}
                label={t('GAME_CONTROL_ENGLISH')}
                actualValue={settings.language}
                labelPosition="aside" value={LANGUAGE.ENGLISH}
                checked={settings.language === LANGUAGE.ENGLISH}
                noDebounce />
            </div>

            <div>
              <Input
                type="radio"
                name="boardtype"
                myOnChange={this.onBoardTypeChange}
                label={t('GAME_CONTROL_CIRCLE')}
                actualValue={settings.boardSettings.type}
                labelPosition="aside" value={circle}
                checked={settings.boardSettings.type === circle}
                noDebounce />
              <Input
                type="radio"
                name="boardtype"
                myOnChange={this.onBoardTypeChange}
                label={t('GAME_CONTROL_SQUARE')}
                actualValue={settings.boardSettings.type}
                labelPosition="aside" value={square}
                checked={settings.boardSettings.type === square}
                noDebounce />
            </div>

            <Input
              type="number"
              myOnChange={this.onBoardSizeChange}
              label={t('GAME_CONTROL_BOARD_SIZE')}
              actualValue={settings.boardSettings.size}
              labelPosition="above" />

            <div style={{
              width: '100%',
              borderBottom: '2px solid MediumSeaGreen',
              marginTop: spacing.LARGE,
              marginBottom: spacing.LARGE
            }} />

            <form onSubmit={this.startMoving}>
              <Input
                type="text"
                label={t('GAME_CONTROL_PLAYER_MOVES_LABEL')}
                actualValue={this.state.moves}
                value={this.state.moves}
                labelPosition="above"
                placeholder={t('GAME_CONTROL_PLAYER_MOVES_NO_MOVES')}
                disabled />

              <div style={{
                ...Row1Col4,
                marginBottom: spacing.LARGE
              }}>
                {
                  this.moveKeys.map((move: string, index: number) => {
                    return this.renderMoves(move, index)
                  })
                }
              </div>

              <button type="submit"
                style={{
                  float: 's',
                  padding: spacing.LARGE,
                  border: '2px solid indianred',
                  borderRadius: '2px',
                  cursor: 'pointer'
                }}>
                {t('GAME_CONTROL_PLAYER_START_MOVING')}
              </button>
            </form>

            <div style={{
              width: '100%',
              borderBottom: '2px solid MediumSeaGreen',
              marginTop: spacing.LARGE,
              marginBottom: spacing.LARGE
            }} />


            {/* <Input
              type="text"
              label={t('GAME_CONTROL_PLAYER_START_Y')}
              actualValue={this.state.moves}
              value={this.state.moves}
              labelPosition="above"
              placeholder={t('GAME_CONTROL_PLAYER_MOVES_NO_MOVES')}
              disabled />

            <Input
              type="text"
              label={t('GAME_CONTROL_PLAYER_START_X')}
              actualValue={this.state.moves}
              value={this.state.moves}
              labelPosition="above"
              placeholder={t('GAME_CONTROL_PLAYER_MOVES_NO_MOVES')}
              disabled /> */}
          </div>
        }
      </div>

    );
  }
}

export default connect<Props>(
  state => ({
    settings: state.settings,
    game: state.game,
    player: state.player
  } as any),
  dispatch => ({
    setLanguage: (lanugage: LANGUAGE) => dispatch(setLanguage(lanugage)),
    setSize: (size: number) => dispatch(setSize(size)),
    setBoardSettings: (boardSettings: Api.BoardSettings) => dispatch(setBoardSettings(boardSettings)),
    getBoard: (gameSettings: Api.BoardSettings) => dispatch(getBoard(gameSettings)),
    getPlayer: (player?: Api.Player) => dispatch(getPlayer(player)),
    movePlayer: (turn: Api.Turn) => dispatch(movePlayer(turn))
  })
)(Control);
