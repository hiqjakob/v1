import * as React from 'react';

class Loader extends React.Component {

  render() {
    return (
      <div style={{
        position: 'relative',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%',
        height: '100%',
        padding: '10px',
      }}>
        Loaaaaaaading...
      </div>
    );
  }
}

export default Loader;
