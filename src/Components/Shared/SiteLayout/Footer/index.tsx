import * as React from 'react';
// import { styled } from 'styletron-react';
import * as spacing from 'Settings/spacing';
import { connect } from 'Store';


type Props = {
  player?: Api.Player
};

// const Footer: any = styled('footer', () => ({
// }));

const footerCssProps: React.CSSProperties = {
  position: 'fixed',
  bottom: 0,
  width: '100%',

  backgroundColor: 'OliveDrab',
  padding: spacing.HUGE
};

class FooterC extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }


  render() {
    let { currentTurn } = this.props.player
    return (
      <footer style={{ ...footerCssProps }}>
        {`Player position: X${currentTurn.position.x}Y${currentTurn.position.y}.
        Facing: ${currentTurn.looking}`}

      </footer>
    );
  }
}

export default connect<Props>(
  state => ({
    player: state.player.player
  } as any)

)(FooterC);
