import * as React from 'react';
// import { styled } from 'styletron-react';
import { NavLink } from 'react-router-dom';
import * as spacing from 'Settings/spacing';
import { withRouter } from 'react-router';
import * as base from 'Settings/base';
// import Heading from 'Core/Heading';
// import Text from 'Core/Text';

// import { createComponent } from 'react-fela';

// Grid istället här
const ulCssProps: React.CSSProperties = {
  listStyle: 'none',
  display: 'flex',
  width: '100%',
  margin: 0,
  padding: 0
};

const liCssProps: React.CSSProperties = {
  padding: spacing.LARGE,
  background: 'purple',
  marginRight: spacing.LARGE,
  borderRadius: spacing.MEDIUM,
  boxShadow: '5px 3px black',
};

const linkCssProps: React.CSSProperties = {
  width: '100%',
  height: base.MEDIUM_ROW,
  padding: spacing.LARGE,
  paddingBottom: 0,
  textTransform: 'uppercase',
  color: 'white',
  fontSize: '24px',
  // backgroundColor: color.SECONDARY_LIGHT,

};

const headerCssProps: React.CSSProperties = {
  margin: `${spacing.LARGE} 0`,
  // border: `${spacing.THINK} solid black`,
  // borderRadius: spacing.MEDIUM,
  // padding: spacing.TINY
};

const activeLinkCssProps: React.CSSProperties = {
  ':link': {
    textDecoration: 'none'
  },
  ':visited': {
    textDecoration: 'none'
  },

  ':hover': {
    textDecoration: 'underline'
  },

  ':active': {
    textDecoration: 'underline'
  }
};

type Props = {
};

class HeaderC extends React.PureComponent<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (

      <header style={{ ...headerCssProps }}>
        <nav>
          <ul style={{ ...ulCssProps }}>
            <li style={{ ...liCssProps }}>
              <NavLink to="/game" style={linkCssProps} activeStyle={activeLinkCssProps}>
                Game
              </NavLink>
            </li>
          </ul>
        </nav>
      </header >
    );
  }
}

export default withRouter(HeaderC as any);
