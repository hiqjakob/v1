import * as React from 'react';
// import { styled } from 'styletron-react';
import { Route, Switch } from 'react-router-dom';
import Game from 'Components/Site/Game';
import Section from '../Section';

class MainC extends React.Component {

  render() {
    return (
      <Section sectionType="SECTION">
        <Switch>
          <Route exact path="/game" component={Game} />
        </Switch>
      </Section>
    );
  }
}

export default MainC;
