import * as React from 'react';
// import { styled } from 'styletron-react';
// import * as color from 'Settings/color';
import * as spacing from 'Settings/spacing';
// import * as base from 'Settings/base';
import Header from './Header';
import Main from './Main';
import Footer from './Footer';
import { BrowserRouter as Router } from 'react-router-dom';
import { createComponent } from 'react-fela';
// import * as spacing from 'Settings/spacing';

const container: any = () => ({
  backgroundColor: 'MediumSeaGreen',
  height: '1200px',
  maxHeight: '1200px'
});

const divCssProps: React.CSSProperties = {
  padding: spacing.LARGE
};

const Container: any = createComponent(container);

class SiteLayout extends React.Component {

  render() {
    return (
      <Router>
        <Container>
          <div style={{ ...divCssProps }}>

            <Header />
            <Main />
          </div>
          <Footer />
        </Container>
      </Router>
    );
  }
}

export default SiteLayout;
