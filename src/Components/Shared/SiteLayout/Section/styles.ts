import * as spacing from 'Settings/spacing';

export type SectionType = 'SECTION' | 'SECTION2';

const SECTION: React.CSSProperties = {
  width: '100%',
  minHeight: '100vh',
  padding: spacing.LARGE,
  backgroundColor: 'Moccasin'
};

const SECTION2: React.CSSProperties = {
  width: '200px',
  height: '200px',
  backgroundColor: 'red '
};
export default function getStyle(sectionType: SectionType): React.CSSProperties {
  switch (sectionType) {
    case 'SECTION':
      return SECTION;
    case 'SECTION2':
      return SECTION2;
    default:
      return {};
  }
}

// class FelaComponent<Props>{
//   props: Props;
//   constructor(props: Props) {

//     this.props = props;
//   }
//   greet() {
//     return "Hello, " + this.greeting;
//   }
// }

// let greeter = new Greeter("world");
