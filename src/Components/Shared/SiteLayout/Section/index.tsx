import * as React from 'react';
import { createComponent, FelaHtmlComponent, Style } from 'react-fela';
import getStyle, { SectionType } from './styles';

interface Props {
  sectionType: SectionType;
  children?: React.ReactChild;
}

const section: Style<SectionProps> = (props: SectionProps) => ({
  ...props.cssProps
});

interface SectionProps {
  cssProps: React.CSSProperties;
}

const Section: FelaHtmlComponent<SectionProps, HTMLElement> = createComponent(section, 'section');

class SectionC extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <Section cssProps={getStyle(this.props.sectionType)} >
        {this.props.children}
      </ Section>

    );
  }
}

export default SectionC;
