import * as React from 'react';
// import { Row2Col1, Row1Col2 } from 'Styles/Grid';
// import * as spacing from 'Settings/spacing';

type Props = {
  point: Api.Point,
  gameSize: number;
  multiplier: number;
  zIndex?: number
};

class Point extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  position() {
    // console.log('position');
  }

  render() {
    let y: number = this.props.multiplier * this.props.point.position.y + (this.props.gameSize / 2);
    let x: number = this.props.multiplier * this.props.point.position.x + (this.props.gameSize / 2);

    return (
      <div style={{
        position: 'absolute',
        top: `${y}px`,
        left: `${x}px`,
        height: `${this.props.multiplier}px`,
        width: `${this.props.multiplier}px`,
        border: '1px solid GoldenRod',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'lightgray',
        fontSize: '10px'
      }}>
        {/* <div style={{ ...Row2Col1, position: 'absolute' }} >
          <div id="north" />
          <div id="south" />
        </div>
        <div style={{ ...Row1Col2, position: 'absolute' }} >
          <div id="west" />
          <div id="east" />
        </div>
        {`${this.props.point.position.x}, ${this.props.point.position.y}`} */}
      </div >
    );
  }
}
// mapDispatchToProps ?: (dispatch: Dispatch<TOwnProps>) => any,

export default Point;
