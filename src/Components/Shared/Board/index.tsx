import * as React from 'react';
import Point from './Point';
import { connect } from 'Store';
import Player from '../Player';
import { getBoard, IState as Game } from 'Store/ducks/game';
import { IState as Settings } from 'Store/ducks/settings';
import { NETWORK_STATE, } from 'Store/ducks/constants';
import Loader from 'Components/Shared/Loader';
import * as _ from 'lodash';



type Props = {
  getBoard?: (BoardSettings: Api.BoardSettings) => void;
  game?: Game;
  settings?: Settings;
};

type State = {
  isLoading: boolean;
}

class Board extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { isLoading: true }
  }

  componentDidMount() {
    this.props.getBoard({
      size: 10,
      type: 'square',
      tolerance: 0
    })
  }

  componentWillReceiveProps(nextProps: Props) {
    let getBoardLoading: boolean = nextProps.game.network.getGame === NETWORK_STATE.LOADING;
    let languageLoading: boolean = nextProps.settings.network.setLanguage === NETWORK_STATE.LOADING;
    let setSizeLoading: boolean = nextProps.settings.network.setSize === NETWORK_STATE.LOADING;
    let setBoardSettingsLoading: boolean = nextProps.settings.network.setBoardSettings === NETWORK_STATE.LOADING;

    let isLoading: boolean = getBoardLoading || languageLoading || setSizeLoading || setBoardSettingsLoading;

    this.setState({ isLoading })

  }

  shouldComponentUpdate(nextProps: Props, nextState: State) {
    false && console.log(nextState);
    let gameEqual: boolean = !_.isEqual(nextProps.game, this.props.game);
    let settingsEqual: boolean = !_.isEqual(nextProps.settings, this.props.settings);
    let isLoadingChanged: boolean = nextState.isLoading !== this.state.isLoading;
    // console.log('isLoadingChanged: ' + isLoadingChanged)

    let shouldUpdate = (gameEqual || settingsEqual) && isLoadingChanged;

    console.log(shouldUpdate);
    return shouldUpdate
  }

  renderBoard(points: Api.Point[][], size: number, multiplier: number) {

    return (
      points.map((xArray: Api.Point[]) => {
        return xArray.map((point: Api.Point) => (
          <Point
            key={point.id}
            gameSize={size}
            multiplier={multiplier}
            point={point}
          />
        ));
      })
    )
  }

  render() {
    let { game, settings } = this.props;
    let size: number = 0;
    // console.log('RENDER - settings, size: ' + settings.size)
    // console.log('RENDER - LOADING: ' + this.state.isLoading);
    console.log('RENDER BOARD')
    console.log('')



    size = (settings.boardSettings.type === 'circle' ?
      settings.boardSettings.size * 2 :
      settings.boardSettings.size) * settings.size + (settings.size * 4);;

    return (
      <div>
        <div style={{
          maxHeight: '500px',
          height: '500px',
          overflow: 'auto',
          backgroundColor: 'IndianRed',
          border: '2px solid GoldenRod',
          borderRadius: '4px'
        }}>

          {this.state.isLoading ?
            <Loader />
            :

            <div style={{
              position: 'relative',
              height: size === 0 ? '100%' : `${size}px`,
              width: size === 0 ? '100%' : `${size}px`
            }}  >
              {(game.game !== undefined && game.game.board !== undefined) && this.renderBoard(game.game.board.points, size, settings.size)}
              <Player multiplier={settings.size} gameSize={size} />
            </div>

          }


        </div>

      </div>
    );
  }
}

export default connect<Props>(
  state => ({
    game: state.game,
    settings: state.settings
  } as any),
  dispatch => ({
    getBoard: (gameSettings: Api.BoardSettings) => dispatch(getBoard(gameSettings))
  })
)(Board);
