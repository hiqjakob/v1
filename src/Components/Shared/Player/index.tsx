import * as React from 'react';
import { connect } from 'Store';
// import * as _ from 'lodash';

import { getPlayer, IState as ReduxPlayer } from 'Store/ducks/player';


type Props = {
  player?: ReduxPlayer;
  getPlayer?: (player?: Api.Player) => void;
  multiplier: number;
  gameSize: number;
};

class Player extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  componentDidMount() {
    this.props.getPlayer()
  }

  getRotation = (looking: Api.Direction): string => {
    let deg: number = 0;
    // console.log(looking);
    switch (looking) {
      case 'north':
        deg = 0;
        break;
      case 'east':
        deg = 90;
        break;
      case 'south':
        deg = 180;
        break;
      case 'west':
        deg = 270;
        break;
      default:
        break;
    }
    return `rotate(${deg}deg)`;

  }
  render() {
    let { player, multiplier, gameSize } = this.props;

    let borderWidth: number = 2;

    let y: number = multiplier * -player.player.currentTurn.position.y + (gameSize / 2) + (multiplier / 4);
    let x: number = multiplier * player.player.currentTurn.position.x + (gameSize / 2) + (multiplier / 4);


    return (
      <div style={{
        position: 'absolute',
        top: `${y}px`,
        left: `${x}px`,
        borderRadius: `${borderWidth}px`,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        color: 'lightgray',
        fontSize: '10px',
        transform: this.getRotation(player.player.currentTurn.looking),
        width: 0,
        height: 0,
        borderLeft: `${(this.props.multiplier / 4) - borderWidth}px solid transparent`,
        borderRight: `${(this.props.multiplier / 4) - borderWidth}px solid transparent`,
        borderBottom: `${(this.props.multiplier / 2) - (borderWidth)}px solid PapayaWhip`,
      }} />
    );
  }
}

export default connect<Props>(
  state => ({
    player: state.player
  } as any),
  dispatch => ({
    getPlayer: (player?: Api.Player) => dispatch(getPlayer(player))
  })
)(Player);
