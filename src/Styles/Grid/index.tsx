
import * as spacing from 'Settings/spacing';
const Grid: React.CSSProperties = {
  display: 'grid',
  width: '100%',
  height: '100%',
};

export const Row2Col1: React.CSSProperties = {
  ...Grid,
  gridTemplateColumns: '100%',
  gridTemplateRows: '50% 50%',
  gridColumnGap: spacing.LARGE,
  height: 'auto'

};

export const Row1Col2: React.CSSProperties = {
  ...Grid,
  gridTemplateColumns: '70% 30%',
  gridTemplateRows: '100%',
  gridColumnGap: spacing.HUGE

};

export const Row1Col4: React.CSSProperties = {
  ...Grid,
  gridTemplateColumns: 'repeat(3, 1fr)',
  gridTemplateRows: '100%',
  gridColumnGap: spacing.LARGE
}


export const Row1Col2_auto: React.CSSProperties = {
  ...Grid,
  gridTemplateColumns: 'auto auto',
};

export const Row1Col1: React.CSSProperties = {
  ...Grid,
  gridTemplateColumns: '100%',
  gridTemplateRows: '100%'
};
