
export const NONE: string = '0';
export const THIN: string = '1px';
export const THINK: string = '2px';
export const TINY: string = '0.125rem';
export const SMALL: string = '0.25rem';
export const MEDIUM: string = '0.5rem';
export const LARGE: string = '1rem';
export const HUGE: string = '2rem';
export const GIGANTIC: string = '3rem';
export const MONSTROUS: string = '4rem';
export const AUTO: string = 'auto';

export type Spacings = 'NONE' | 'THIN' | 'THINK' | 'TINY' | 'SMALL' | 'MEDIUM' | 'LARGE' | 'HUGE' | 'GIGANTIC' | 'AUTO' | 'MONSTROUS';
