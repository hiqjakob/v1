
export const SMALLEST_LINE_HEIGHT: number = 1;
export const SMALLER_LINE_HEIGHT: number = 1.1;
export const SMALL_LINE_HEIGHT: number = 1.2;
export const BASE_LINE_HEIGHT: number = 1.3;
export const LARGE_LINE_HEIGHT: number = 1.4;
export const LARGER_LINE_HEIGHT: number = 1.5;
export const LARGEST_LINE_HEIGHT: number = 1.6;

export type PaddingTypes =
    'SMALLEST_LINE_HEIGHT'
    | 'SMALLER_LINE_HEIGHT'
    | 'SMALL_LINE_HEIGHT'
    | 'BASE_LINE_HEIGHT'
    | 'LARGE_LINE_HEIGHT'
    | 'LARGER_LINE_HEIGHT'
    | 'LARGEST_LINE_HEIGHT';
