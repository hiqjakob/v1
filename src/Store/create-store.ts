// import { createStore, applyMiddleware, Middleware, StoreEnhancerStoreCreator } from 'redux';
import { createStore, applyMiddleware, StoreEnhancerStoreCreator } from 'redux';

import { composeWithDevTools } from 'redux-devtools-extension';
import * as Store from './index';
import createSagaMiddleWare, { SagaMiddleware } from 'redux-saga';
// import { createLogger } from 'redux-logger';
// import reducer from './reducers';
export default function (initialState: Store.ReduxState): Store.Store {
    const sagaMiddleware: SagaMiddleware<{}> = createSagaMiddleWare();

    // const logger: Middleware = createLogger({
    //     predicate: (_, action) => {
    //         // Remove all redux-form from log
    //         return action.type.indexOf('@@reduxform') === -1 &&
    //             action.type.indexOf('globalNetwork') === -1;
    //     }
    // });

    const createStoreWithMiddleware: StoreEnhancerStoreCreator<{}> = composeWithDevTools(applyMiddleware(sagaMiddleware /*, logger */)
    )(createStore);

    const reduxStore: Store.Store = createStoreWithMiddleware(Store.reducers, initialState);

    // const reduxStore: any = createStore<Store.ReduxState>(reducer, composeWithDevTools(applyMiddleware(sagaMiddleware /*globalNetwork, logger */)));

    // window.store = reduxStore;

    sagaMiddleware.run(Store.sagas);
    return reduxStore;
}
