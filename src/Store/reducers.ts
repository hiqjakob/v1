import { ReduxState } from './index';
import GameReducer from './ducks/game';
import PlayerReducer from './ducks/player';
import SettingsReducer from './ducks/settings';

import deepFreeze from './deep-freeze';

export default (state: ReduxState, action: Api.Action): ReduxState => {
    return deepFreeze({
        game: GameReducer(state.game, action),
        player: PlayerReducer(state.player, action),
        settings: SettingsReducer(state.settings, action)
    });
};
