
export default function deepFreeze(o: any): Object {
  if (Array.isArray(o)) {
    o.forEach((i: number) => deepFreeze(o[i]));
  } else if (typeof o === 'object' && o !== undefined) {

    (Object.keys(o as {}) as string[]).map((key: string) => deepFreeze(o[key]));
    // Object.keys(o).forEach((key: string) => deepFreeze(o[key]));
  }
  Object.freeze(o);

  return o;
}
