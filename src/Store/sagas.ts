import { fork } from 'redux-saga/effects';
import { sagas as gameSaga } from './ducks/game';
import { sagas as playerSaga } from './ducks/player';
import { sagas as settingsSaga } from './ducks/settings';

export default function* sagas() {
  yield fork(gameSaga);
  yield fork(playerSaga);
  yield fork(settingsSaga);
}
