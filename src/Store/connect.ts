import { } from 'react-redux';
import { Store } from 'redux';
import { ReduxState } from './index';
import { connect, MergeProps, Options, AdvancedComponentDecorator, Dispatch } from 'react-redux';

interface MapStateToProps<TStateProps, TOwnProps> {
    (state: ReduxState, ownProps?: TOwnProps): TStateProps;
}

export type Store = Store<ReduxState>;

export default function <TOwnProps>(
    mapStateToProps: MapStateToProps<ReduxState, TOwnProps>,
    mapDispatchToProps?: (dispatch: Dispatch<TOwnProps>) => any,
    mergeProps?: MergeProps<ReduxState, any, TOwnProps, any>,
    options?: Options
): AdvancedComponentDecorator<{}, TOwnProps> {
    return connect.apply(this, [mapStateToProps, mapDispatchToProps, mergeProps, options]);
}
