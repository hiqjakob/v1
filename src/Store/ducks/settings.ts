// import { call, put, takeLatest } from 'redux-saga/effects';
import { put, takeLatest } from 'redux-saga/effects';
import { NETWORK_STATE } from './constants';
// import * as API from 'Infrastructure/API';

// Actions
const SET_SIZE: string = 'api/settings/set-size';
const SET_SIZE_STARTED: string = 'api/settings/set-size/loading';
const SET_SIZE_SUCCEEDED: string = 'api/settings/set-size/succeeded';
const SET_SIZE_FAILED: string = 'api/settings/set-size/failed';

const SET_LANGUAGE: string = 'api/settings/set-language';
const SET_LANGUAGE_STARTED: string = 'api/settings/set-language/loading';
const SET_LANGUAGE_SUCCEEDED: string = 'api/settings/set-language/succeeded';
const SET_LANGUAGE_FAILED: string = 'api/settings/set-language/failed';

const SET_BOARD_SETTINGS: string = 'api/settings/set-board-settings';
const SET_BOARD_SETTINGS_STARTED: string = 'api/settings/set-board-settings/loading';
const SET_BOARD_SETTINGS_SUCCEEDED: string = 'api/settings/set-board-settings/succeeded';
const SET_BOARD_SETTINGS_FAILED: string = 'api/settings/set-board-settingss/failed';

export enum LANGUAGE {
  ENGLISH = 0,
  SWEDISH = 1
}

export interface IState {
  language: LANGUAGE;
  size: number;
  boardSettings: Api.BoardSettings;
  network: Network;
}

export interface Network {
  setLanguage: NETWORK_STATE;
  setSize: NETWORK_STATE;
  setBoardSettings: NETWORK_STATE;
}

// Initial State
const initialState: IState = {
  language: LANGUAGE.SWEDISH,
  boardSettings: {
    size: 10,
    tolerance: 0,
    type: 'square'
  },
  size: 20,
  network: {
    setLanguage: NETWORK_STATE.IDLE,
    setSize: NETWORK_STATE.IDLE,
    setBoardSettings: NETWORK_STATE.IDLE
  }
};

export default function reducer(state: IState = initialState, action: Api.Action<any>) {

  switch (action.type) {
    case SET_LANGUAGE_STARTED:
      return {
        ...state,
        network: {
          ...state.network,
          setLanguage: NETWORK_STATE.LOADING
        }
      }
    case SET_LANGUAGE_SUCCEEDED:
      return {
        ...state,
        language: action.payload,
        network: {
          ...state.network,
          setLanguage: NETWORK_STATE.IDLE
        }
      }

    case SET_LANGUAGE_FAILED:
      return {
        ...state,
        network: {
          ...state.network,
          setLanguage: NETWORK_STATE.ERROR
        }
      }

    case SET_SIZE_STARTED:
      return {
        ...state,
        network: {
          ...state.network,
          setLanguage: NETWORK_STATE.LOADING
        }
      }
    case SET_SIZE_SUCCEEDED:
      return {
        ...state,
        size: action.payload,
        network: {
          ...state.network,
          setLanguage: NETWORK_STATE.IDLE
        }
      }
    case SET_SIZE_FAILED:
      return {
        ...state,
        network: {
          ...state.network,
          setLanguage: NETWORK_STATE.ERROR
        }
      }

    case SET_BOARD_SETTINGS_STARTED:
      return {
        ...state,
        network: {
          ...state.network,
          setBoardSettings: NETWORK_STATE.LOADING
        }
      }
    case SET_BOARD_SETTINGS_SUCCEEDED:
      return {
        ...state,
        boardSettings: {
          ...state.boardSettings,
          ...action.payload
        },
        network: {
          ...state.network,
          setBoardSettings: NETWORK_STATE.IDLE
        }
      }
    case SET_BOARD_SETTINGS_FAILED:
      return {
        ...state,
        network: {
          ...state.network,
          setBoardSettings: NETWORK_STATE.ERROR
        }
      }

    default:
      return state;
  }
}

// Sagas
export function* setLanguageSaga(action: Api.Action<LANGUAGE>) {
  try {
    yield put({ type: SET_LANGUAGE_STARTED } as Api.Action);
    yield put({ type: SET_LANGUAGE_SUCCEEDED, payload: action.payload } as Api.Action<LANGUAGE>);
  } catch (e) {
    console.log(action);
    console.log(e);
    yield put({ type: SET_LANGUAGE_FAILED } as Api.Action);
  }
}

export function* setSizeSaga(action: Api.Action<number>) {
  try {
    yield put({ type: SET_SIZE_STARTED } as Api.Action);
    yield put({ type: SET_SIZE_SUCCEEDED, payload: action.payload } as Api.Action<Number>);
  } catch (e) {
    console.log(action);
    console.log(e);
    yield put({ type: SET_SIZE_FAILED } as Api.Action);
  }
}

export function* setBoardSettingsSaga(action: Api.Action<Api.BoardSettings>) {
  try {
    yield put({ type: SET_BOARD_SETTINGS_STARTED } as Api.Action);
    yield put({ type: SET_BOARD_SETTINGS_SUCCEEDED, payload: action.payload } as Api.Action<Api.BoardSettings>);
  } catch (e) {
    console.log(action);
    console.log(e);
    yield put({ type: SET_SIZE_FAILED } as Api.Action);
  }
}

export function* sagas() {
  yield takeLatest(SET_LANGUAGE, setLanguageSaga);
  yield takeLatest(SET_SIZE, setSizeSaga);
  yield takeLatest(SET_BOARD_SETTINGS, setBoardSettingsSaga);

}

// action-creators
export const setLanguage = (language: LANGUAGE) => ({
  type: SET_LANGUAGE,
  payload: language
} as Api.Action<LANGUAGE>);

// action-creators
export const setSize = (size: number) => ({
  type: SET_SIZE,
  payload: size
} as Api.Action<number>);

export const setBoardSettings = (boardSettings: Api.BoardSettings) => ({
  type: SET_BOARD_SETTINGS,
  payload: boardSettings
} as Api.Action<Api.BoardSettings>)

