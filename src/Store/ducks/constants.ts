export enum NETWORK_STATE {
  IDLE = 0,
  LOADING = 1,
  ERROR = 2,
}
