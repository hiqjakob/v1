import { takeEvery, takeLatest, put } from 'redux-saga/effects';
import { NETWORK_STATE } from './constants';
import Player from 'Store/initialData/Player';
// import { delay } from 'redux-saga'

const MOVE_PLAYER: string = 'api/player/move';
const MOVE_PLAYER_STARTED: string = 'api/player/move/loading';
const MOVE_PLAYER_SUCCEEDED: string = 'api/player/move/succeeded';
const MOVE_PLAYER_FAILED: string = 'api/player/move/failed';

const GET_PLAYER: string = 'api/player/get-player';
const GET_PLAYER_STARTED: string = 'api/player/get-player/loading';
const GET_PLAYER_SUCCEEDED: string = 'api/player/get-player/succeeded';
const GET_PLAYER_FAILED: string = 'api/player/get-player/failed';


export interface IState {
  player: Api.Player;
  // history: Api.Turn[];
  network: Network;
}


export interface Network {
  get: NETWORK_STATE;
  move: NETWORK_STATE;
  // addMoves: NETWORK_STATE;
}
// Hjälp klass så jag slipper ha funktionallitet här, också att jag kan enklare testa. 
let MyPlayer: Player = new Player();

// Initial State

// },
const initialState: IState = {
  player: MyPlayer.player,
  network: {
    get: NETWORK_STATE.IDLE,
    move: NETWORK_STATE.IDLE,
  }
};

export default function reducer(state: IState = initialState, action: Api.Action) {

  switch (action.type) {
    case MOVE_PLAYER_STARTED: {
      return {
        ...state,
        network: {
          ...state.network,
          move: NETWORK_STATE.LOADING
        }
      };
    }
    case MOVE_PLAYER_SUCCEEDED: {
      let my = {
        ...state,
        player: {
          ...state.player,
          currentTurn: { ...state.player.currentTurn, ...action.payload },
        },
        network: {
          ...state.network,
          move: NETWORK_STATE.IDLE
        }
      };
      return my;
    }
    case MOVE_PLAYER_FAILED: {
      return {
        ...state,
        network: {
          ...state.network,
          move: NETWORK_STATE.ERROR
        }
      };
    }

    case GET_PLAYER_STARTED: {
      return {
        ...state,
        network: {
          ...state.network,
          get: NETWORK_STATE.LOADING
        }
      };
    }
    case GET_PLAYER_SUCCEEDED: {
      console.log('GETPLAYER')
      console.log(action.payload)
      return {
        ...state,
        player: {
          ...state.player,
          ...action.payload
        },
        network: {
          ...state.network,
          get: NETWORK_STATE.IDLE
        }
      };
    }
    case GET_PLAYER_FAILED: {
      return {
        ...state,
        network: {
          ...state.network,
          get: NETWORK_STATE.ERROR
        }
      };
    }
    default:
      return state;
  }
}

// Sagas
export function* movePlayerSaga(action: Api.Action<Api.Turn>) {
  try {
    yield put({ type: MOVE_PLAYER_STARTED } as Api.Action);
    // yield call(delay, 5000) // för att få se rörelserna
    yield put({ type: MOVE_PLAYER_SUCCEEDED, payload: action.payload } as Api.Action<Api.Turn>);
  } catch (e) {
    yield put({ type: MOVE_PLAYER_FAILED } as Api.Action);
  }
}

// export function* addMovesSaga() {
//   try {
//     yield put({ type: ADD_MOVES_STARTED } as Api.Action<Api.Turn>);
//     yield put({ type: ADD_MOVES_SUCCEEDED } as Api.Action<Api.Turn>);
//   } catch (e) {
//     yield put({ type: ADD_MOVES_FAILED } as Api.Action<Api.Turn>);
//   }
// }

export function* getPlayerSaga(action: Api.Action<Api.Player>) {
  try {
    yield put({ type: GET_PLAYER_STARTED } as Api.Action);
    yield put({ type: GET_PLAYER_SUCCEEDED, payload: action.payload } as Api.Action<Api.Player>);
  } catch (e) {
    yield put({ type: GET_PLAYER_FAILED } as Api.Action);
  }
}

export function* sagas() {
  yield takeEvery(MOVE_PLAYER, movePlayerSaga);
  // yield takeLatest(ADD_MOVES, addMovesSaga);
  yield takeLatest(GET_PLAYER, getPlayerSaga);
}

// action-creators
export const movePlayer = (turn: Api.Turn) => ({
  type: MOVE_PLAYER,
  payload: turn
}) as Api.Action<Api.Turn>;

// export const addMoves = (turns: Api.Turn[]) => ({
//   type: ADD_MOVES,
//   payload: turns
// } as Api.Action<Api.Turn[]>);

export const getPlayer = (player?: Api.Player) => ({
  type: GET_PLAYER,
  payload: player !== undefined ? player : MyPlayer.player
} as Api.Action<Api.Player>);
