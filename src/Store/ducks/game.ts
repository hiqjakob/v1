// import { call, put, takeLatest } from 'redux-saga/effects';
import { put, takeLatest, call } from 'redux-saga/effects';
import { NETWORK_STATE } from './constants';
// import * as API from 'Infrastructure/API';
import Game from 'Store/initialData/game';
import { delay } from 'redux-saga'
// export const delay = call.bind(null, delayUtil)
// Actions
const GET_BOARD: string = 'api/board';
const GET_BOARD_STARTED: string = 'api/board/loading';
const GET_BOARD_SUCCEEDED: string = 'api/board/succeeded';
const GET_BOARD_FAILED: string = 'api/board/failed';

export interface IState {
  game: Api.Game;
  network: Network;
}

export interface Network {
  getGame: NETWORK_STATE;
}


// Hjälp klass så jag slipper ha funktionallitet här, också att jag kan enklare testa. 
let MyGame: Game = new Game();
// Initial State
const initialState: IState = {
  game: MyGame.game,
  network: {
    getGame: NETWORK_STATE.IDLE
  }
};

export default function reducer(state: IState = initialState, action: Api.Action) {
  switch (action.type) {

    case GET_BOARD_STARTED:
      return {
        ...state,
        network: {
          ...state.network,
          getGame: NETWORK_STATE.LOADING
        }
      }

    case GET_BOARD_SUCCEEDED:
      return {
        ...state,
        game: { ...state.game, ...action.payload },
        network: {
          ...state.network,
          getGame: NETWORK_STATE.IDLE
        }
      };

    case GET_BOARD_FAILED:
      return {
        ...state,
        network: {
          ...state.network,
          getGame: NETWORK_STATE.ERROR
        }
      };

    default:
      return state;
  }
}

export function getGame() {
  return async function () {
    let game = await MyGame.getNewGame()
    return game;
  }
}

export function* gameListSaga(action: Api.Action<Api.BoardSettings>) {
  try {
    yield put({ type: GET_BOARD_STARTED } as Api.Action);
    MyGame.setUpValues(action.payload);
    // diskutera gärna här
    let game: Api.Game = yield call(getGame());
    yield call(delay, 500) // för att få laddningen att synas

    yield put({ type: GET_BOARD_SUCCEEDED, payload: game } as Api.Action<Api.Game>);

  } catch (e) {
    console.log('Error Error Error');
    console.log(action);
    console.log(e);
    yield put({ type: GET_BOARD_FAILED } as Api.Action);
  }
}

export function* sagas() {
  yield takeLatest(GET_BOARD, gameListSaga);
}

// action-creators
export const getBoard = (boardSettings: Api.BoardSettings) => ({
  type: GET_BOARD,
  payload: boardSettings
} as Api.Action<Api.BoardSettings>);

