// import Point from "src/Components/Shared/Board/Point";


class Player {
  player: Api.Player;
  history: Api.Turn[];

  constructor() {
    this.player = {
      id: 1,
      currentTurn: {
        turn: 1,
        looking: 'east',
        position: {
          x: 0,
          y: 0
        },
      }
    };
    this.history = []
  }
}

export default Player;