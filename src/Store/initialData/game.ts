// import Point from "src/Components/Shared/Board/Point";


class Game {
  game: Api.Game;
  settings: Api.BoardSettings;
  constructor() {
  }

  setUpValues(boardSettings: Api.BoardSettings) {
    this.settings = boardSettings;
  }

  getNewGame(): Promise<Api.Game> {
    let that = this;
    return new Promise<Api.Game>((resolve) => {
      resolve(
        this.game = {
          id: 1,
          turn: 0,
          board: {
            id: 1,
            points: that.getPlane(),
          }
        }
      )
    })
  }

  getPlane(): Api.Point[][] {
    switch (this.settings.type) {
      case 'square':
        return this.getSquarePlane();
      case 'circle':
        return this.getCirlePlane();
      default:
        return []
    }
  }

  getSquarePlane(): Api.Point[][] {
    // let isOdd: boolean = size % 2 !== 0;
    let minValue: number = Math.floor(this.settings.size / 2) * -1;
    let maxValue: number = Math.ceil(this.settings.size / 2);
    let x: number = minValue;
    let y: number = minValue;
    let id: number = 1;
    let plane: Api.Point[][] = [];

    for (y; y < maxValue; y++) {
      let xPoints: Api.Point[] = [];
      for (x; x < maxValue; x++) {
        let point: Api.Point = {
          id,
          position: {
            x,
            y
          }
        };
        xPoints.push(point);
        id++;
      }
      //adding row for row
      plane.push(xPoints);
      x = minValue;
    }
    return plane;
  }

  getCirlePlane(): Api.Point[][] {
    let plane: Api.Point[][] = [];
    let y: number = -this.settings.size;
    let x: number = -this.settings.size;
    let id: number = 1;

    for (y; y <= this.settings.size; y++) {
      let xPoints: Api.Point[] = [];

      for (x; x <= this.settings.size; x++) {
        let distance: number = this.pythagorean(y, x);
        let addPoint: boolean = distance <= this.settings.size + this.settings.tolerance;

        if (addPoint) {
          let point: Api.Point = {
            id,
            position: {
              x,
              y
            }
          }
          xPoints.push(point)
          // console.log(`point X${x} Y${y}, pythagorean ${distance}`)
          id++;
        }
      }
      if (xPoints.length > 0) {
        plane.push(xPoints)
      }
      x = -this.settings.size;
    }

    return plane
  }

  pythagorean(sideA: number, sideB: number) {
    return Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2));
  }
}

export default Game;