import { IState as IGameState } from 'Store/ducks/game';
import { IState as IPlayerState } from 'Store/ducks/player';
import { IState as ISettingsState } from 'Store/ducks/settings';


// import { IState as IInstructionsState } from 'Store/ducks/instructions';

export default interface IReduxState {
  game?: IGameState;
  player?: IPlayerState;
  settings?: ISettingsState;
}
