import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { AppContainer } from 'react-hot-loader';
import App from './App';

// Global FCSS
import './Styles/Global/base.css';
import './Styles/Global/spinner.css';

// require('./Styles/Global/base.css');
// require('./Styles/Global/spinner.css');

const render: any = (Component: any) => {
    ReactDOM.render(
        <AppContainer>
            <Component />
        </AppContainer >
        ,
        document.getElementById('root')
    );
};

if (module.hot) {
    module.hot.accept(() => {
        render(App);
    });
}
render(App);
