import * as React from 'react';

// import { StyletronProvider } from 'styletron-react';

import { Provider } from 'react-redux';
import { Provider as FelaProvider } from 'react-fela';
import { createRenderer, IRenderer } from 'fela';
import { createStore, ReduxState, Store } from 'Store';
import SiteLayout from 'Components/Shared/SiteLayout';

// const styletron = new Styletron([styleElements]);

// Get the application-wide store instance, prepopulating with state from the server where available.

const initialState: ReduxState = {};

export const store: Store = createStore(initialState);
const renderer: IRenderer = createRenderer();

const App: any = () => {
    return (
        <Provider store={store}>
            <FelaProvider renderer={renderer}>
                <SiteLayout />
            </FelaProvider>
        </Provider>
    );
};

export default App;
