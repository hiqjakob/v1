const commonConfig = require('./common');
const WebpackNotifierPlugin = require('webpack-notifier');
const merge = require('webpack-merge');
const proxy = 'localhost:3000';
const paths = require('./paths');
const webpack = require('webpack');

module.exports = function () {
    commonConfig.entry.main.unshift('react-hot-loader/patch');

    return merge(commonConfig, {
        devtool: 'inline-source-map',
        devServer: {
            // Proxy API calls to backend on port 3000, backend not implemented though. Doing everything in FE
            proxy: {
                '/api': {
                    target: 'http://' + proxy,
                    pathRewrite: {
                        '^/api': ''
                    }
                },
                secure: false,
                changeOrigin: true
            },
            port: 8080,
            host: 'localhost',
            hot: true,
            inline: true,
            contentBase: paths.output,
            publicPath: '/dist/',
            noInfo: false,
            overlay: true,
            historyApiFallback: true,
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new WebpackNotifierPlugin({ alwaysNotify: true }),
            new webpack.NamedModulesPlugin()
        ]
    });
}();




