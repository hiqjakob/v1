const defaults = {
    limit: 10000
};

module.exports = function (options) {
    return {
        loader: 'url-loader',
        options: Object.assign({}, defaults, options)
    };
};



