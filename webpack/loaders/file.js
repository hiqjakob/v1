const defaults = {}

module.exports = function (options) {
    return {
        loader: 'file-loader',
        options: Object.assign(defaults, options)
    }
}
