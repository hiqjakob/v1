const ExtractTextPlugin = require('extract-text-webpack-plugin');
const  WebpackNotifierPlugin = require('webpack-notifier');
const merge = require('webpack-merge');
const commonConfig = require('./common');

const dev = merge(commonConfig,{
  devtool: 'inline-source-map',
  plugins: [
    new WebpackNotifierPlugin({alwaysNotify: true}),
    new ExtractTextPlugin({ filename: '[name].css', disable: false, allChunks: true }),
    new webpack.DefinePlugin({
      "process.env": {
          "NODE_ENV": JSON.stringify("development")
      }
  })
  ]
});

module.exports = dev;
