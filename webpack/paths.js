const path = require('path');

const includeClientPath = path.join(__dirname, '..', 'src');
const includeServerPath = path.join(__dirname, '..', 'backend');


module.exports = {
  includeClientPath: includeClientPath,
  includeServerPath: includeServerPath,
  entry: path.join(includeClientPath, 'BrowserEntry.tsx'),
  output: path.join(__dirname, '..', 'dist'),
  nodeModules: path.resolve(__dirname, '..', 'node_modules'),
  htmlIndex: path.resolve(includeClientPath, 'index.ejs')
}
