const webpack = require("webpack");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const AssetsPlugin = require('assets-webpack-plugin');
const TsConfigPathsPlugin = require('awesome-typescript-loader').TsConfigPathsPlugin;
const WriteFilePlugin = require('write-file-webpack-plugin');
const paths = require('./paths');
const vendors = require('./vendors');
const typescript = require('./loaders/typescript');
const image = require('./loaders/image');
const url = require('./loaders/url');
const file = require('./loaders/file');

// var fs = require('fs');
// var nodeModules = {};
// fs.readdirSync('node_modules')
//     .filter(function (x) {
//         return ['.bin'].indexOf(x) === -1;
//     })
//     .forEach(function (mod) {
//         nodeModules[mod] = 'commonjs ' + mod;
//     });


debugger
module.exports = {
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"],
        modules: [paths.includeClientPath, paths.nodeModules]
    },
    module: {

        rules: [
            // All files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'.
            {
                test: /\.tsx?$/,
                use: typescript(),
                include: [paths.includeClientPath],
                exclude: [paths.nodeModules]
            },
            {
                test: /\.(svg| woff2?|eot|ttf)$/i,
                use: [file()]
            },
            {
                test: /\.(svg|png|jpg|jpeg|gif|webp)$/i,
                use: [url()]
            },
            {
                test: /\.css/,
                loaders: ExtractTextPlugin.extract({
                    use: "css-loader"
                }),
            },
            {
                enforce: "pre",
                test: /\.js$/,
                loader: "source-map-loader"
            },
        ],
        noParse: function (content) {
            return /express/.test(content)
        }
    },
    entry: {
        main: [paths.entry],
        vendor: vendors
    },
    output: {
        path: paths.output,
        filename: "[name].js",
        publicPath: "/dist/"
    },
    plugins: [
        new webpack.optimize.CommonsChunkPlugin({
            names: ['vendor'],
            minChunks: Infinity,
        }),
        new HtmlWebpackPlugin({
            title: 'react-hot-ts',
            chunksSortMode: 'dependency',
            template: paths.htmlIndex
        }),
        new WriteFilePlugin(),
        new ExtractTextPlugin("global.css"),
        new AssetsPlugin({
            filename: 'webpack.assets.json',
            path: paths.output,
            prettyPrint: true
        }),
        new TsConfigPathsPlugin()
    ]
};
