
declare module Props {
  type Direction = 'north' | 'east' | 'south' | 'west';

  interface ExampleProps {
    name: string;
    age: number;
    sex?: 'male' | 'female' | '';
  }


}
