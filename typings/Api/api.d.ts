
declare module Api {
  type Direction = 'north' | 'east' | 'south' | 'west';
  type BoardType = 'circle' | 'square'
  type Tolerance = 0 | 0.1 | 0.3 | 0.4 | 0.5 | 0.6;

  interface Game {
    id: number;
    turn: number;
    board: Board;
  }

  interface Board {
    id: number;
    points: Point[][];
  }

  interface BoardSettings {
    size?: number;
    type?: Api.BoardType;
    tolerance?: Tolerance;
  }

  interface Point {
    id: number;
    position: Position;
  }

  interface Position {
    x: number;
    y: number;
  }

  interface Turn {
    turn: number;
    looking: Direction;
    position: Position;
  }

  interface Player {
    id: number;
    currentTurn: Turn;
  }



  // Redux Helpers here: 
  // ...............................................
  interface Action<T = any> {
    type: string;
    payload?: T;
  }

}
